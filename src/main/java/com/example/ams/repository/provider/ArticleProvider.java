package com.example.ams.repository.provider;

import com.example.ams.model.Article;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {

    public String findAll(Integer id){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.created_date,a.thumbnail,a.cat_id,c.name");
            FROM("tb_article a");
            INNER_JOIN("tb_category c on a.cat_id = c.id ");
            if(id!=null){
                WHERE("a.id = "+id);
            }
        }}.toString();
    }

    public String deleteById(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_article");
            WHERE("id = "+id);
        }}.toString();
    }

    public String insert(Article article){
        return new SQL(){{
            INSERT_INTO("tb_article");
            VALUES("title","'"+article.getTitle()+"'");
            VALUES("description","'"+article.getDescription()+"'");
            VALUES("thumbnail","'"+article.getImage()+"'");
            VALUES("author","'"+article.getAuthor()+"'");
            VALUES("created_date","'"+article.getCreatedDate()+"'");
            VALUES("cat_id",""+article.getCategory().getId());
        }}.toString();
    }

    public String updateById(Article article){
        return new SQL(){{
            UPDATE("tb_article");
            SET("title = '"+article.getTitle()+"'");
            SET("description = '"+article.getDescription()+"'");
            SET("thumbnail = '"+article.getImage()+"'");
            SET("author = '"+article.getAuthor()+"'");
            SET("created_date = '"+article.getCreatedDate()+"'");
            SET("cat_id = "+article.getCategory().getId());
            WHERE("id = " + article.getId());
        }}.toString();
    }

    public String filterCategory(){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.created_date,a.thumbnail,a.cat_id,c.name");
            FROM("tb_article a");
            INNER_JOIN("tb_category c on a.cat_id = c.id ");
            WHERE("a.cat_id = #{catId}");
        }}.toString();
    }

    public String findByPage(int page){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.created_date,a.thumbnail,a.cat_id,c.name");
            FROM("tb_article a");
            INNER_JOIN("inner join tb_category c on c.id = a.cat_id  order by a.id desc LIMIT 10 OFFSET "+page);
        }}.toString();
    }

    public String  filterCategoryPage(int cat_id,int page){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.created_date,a.thumbnail,a.cat_id,c.name");
            FROM("tb_article a");
            INNER_JOIN("tb_category c on c.id = a.cat_id where c.id = " + cat_id + " order by a.id desc LIMIT 10 OFFSET "+page);
        }}.toString();
    }

    public String filterByTitle(String title,int cat_id){
        return new SQL(){{
            SELECT("count(*)");
            FROM("tb_article a");
            INNER_JOIN("tb_category c on a.cat_id = c.id");
            WHERE("a.cat_id = "+cat_id+" and a.title ILIKE '%"+title+"%'");
        }}.toString();
    }

    public String filterByTitlePage(int cat_id,String title,int page){
        return "select a.id, a.title, a.description, a.author, a.created_date,a.thumbnail,a.cat_id,c.name FROM tb_article a INNER JOIN tb_category c on c.id = a.cat_id  WHERE a.cat_id = "+cat_id+" and a.title ILIKE '%"+title+"%'  order by a.id desc LIMIT 10 OFFSET "+page;
    }
    
    public String filterAllArticle(String title){
        return new SQL(){{
            SELECT("count(*)");
            FROM("tb_article a");
            INNER_JOIN("tb_category c on a.cat_id = c.id");
            WHERE("a.title ILIKE '%"+title+"%'");
        }}.toString();
    }

    public String filterAllArticleByPage(String title,int page){
        return "select a.id, a.title, a.description, a.author, a.created_date,a.thumbnail,a.cat_id,c.name FROM tb_article a INNER JOIN tb_category c on c.id = a.cat_id  WHERE a.title ILIKE '%"+title+"%'  order by a.id desc LIMIT 10 OFFSET "+page;
    }
}
