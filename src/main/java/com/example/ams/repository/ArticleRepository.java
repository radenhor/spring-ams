package com.example.ams.repository;

import com.example.ams.model.Article;
import com.example.ams.repository.provider.ArticleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ArticleRepository {

    @SelectProvider(method = "findAll",type = ArticleProvider.class)
    @Results({
            @Result(property = "image",column = "thumbnail"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "category.id",column = "cat_id"),
            @Result(property = "category.catName",column = "name"),
    })
    List<Article> findAll();
    @SelectProvider(method = "findAll",type = ArticleProvider.class)
    @Results({
            @Result(property = "image",column = "thumbnail"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "category.id",column = "cat_id"),
            @Result(property = "category.catName",column = "name"),
    })
    Article findById(int id);
    @DeleteProvider(method = "deleteById",type = ArticleProvider.class)
    void deleteById(int id);
    @InsertProvider(method = "insert",type = ArticleProvider.class)
    void add(Article article);
    @UpdateProvider(method = "updateById",type = ArticleProvider.class)
    void update(Article article);
    @Select("select a.id, a.title, a.description, a.author, a.created_date,a.thumbnail,a.cat_id,c.name  from " +
            "tb_article a " +
            "inner join tb_category c " +
            "on c.id = a.cat_id order by a.id desc " +
            "LIMIT 10 OFFSET #{page} ")
    @Results({
            @Result(property = "image",column = "thumbnail"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "category.id",column = "cat_id"),
            @Result(property = "category.catName",column = "name"),
    })
    List<Article> findByPage(int page);
    @Select("select count(*) from " +
            "tb_article a " +
            "inner join tb_category c " +
            "on c.id = a.cat_id " +
            "where c.id = #{catId}")
   Integer filterCategory(int catId);
    @SelectProvider(method = "filterCategoryPage",type = ArticleProvider.class)
    @Results({
            @Result(property = "image",column = "thumbnail"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "category.id",column = "cat_id"),
            @Result(property = "category.catName",column = "name"),
    })
    List<Article> filterCategoryPage(int cat_id,int page);
    @Select("Select count(*) from tb_article")
    Integer allPage();

//    @Select("select a.id, a.title, a.description, a.author, a.created_date,a.thumbnail,a.cat_id,c.name  from " +
//            "tb_article a " +
//            "where a.cat_id = #{cat_id} and a.title like %#{title}%")
    @SelectProvider(method = "filterByTitle",type = ArticleProvider.class)
    Integer filterByTitle(String title,int cat_id);
    @SelectProvider(method = "filterByTitlePage",type = ArticleProvider.class)
    @Results({
            @Result(property = "image",column = "thumbnail"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "category.id",column = "cat_id"),
            @Result(property = "category.catName",column = "name"),
    })
    List<Article> filterByTitlePage(int cat_id,String title,int page);
    @SelectProvider(method = "filterAllArticle",type = ArticleProvider.class)
    Integer filterAllArticle(String title);
    @SelectProvider(method = "filterAllArticleByPage",type = ArticleProvider.class)
    @Results({
            @Result(property = "image",column = "thumbnail"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "category.id",column = "cat_id"),
            @Result(property = "category.catName",column = "name"),
    })
    List<Article> filterAllArticleByPage(String title,int page);
}
