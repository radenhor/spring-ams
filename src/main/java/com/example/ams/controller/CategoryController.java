package com.example.ams.controller;

import com.example.ams.model.Article;
import com.example.ams.model.Category;
import com.example.ams.service.CategoryService;
import com.example.ams.service.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CategoryController {

   
    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public static Integer page = 1;
    @GetMapping("/category")
    public String category(Model model){
        Integer allPage = categoryService.allPage();
        List<Category> subCategory = categoryService.findByPage(page);
        model.addAttribute("category",subCategory);
        model.addAttribute("currentPage",page);
        model.addAttribute("pageSelect",page==1?0:page-1);
        model.addAttribute("page", Pagination.prePageCat(allPage));
        model.addAttribute("enPage",Pagination.convertCatPage(Pagination.prePageCat(allPage)));
//        model.addAttribute("listPage",Pagination.convertPage(Pagination.preparePage(allPage,5)));
        return "ListCategory";
    }

    @GetMapping("/addcategory")
    public String addCategory(Model model){
        model.addAttribute("isAdd",true);
        model.addAttribute("category",new Category());
        return "ModifyCategory";
    }

    @PostMapping("/addcategory")
    public String addCategory(@Valid @ModelAttribute("category") Category category,  BindingResult bindingResult1, Model model){
        if(bindingResult1.hasErrors()){
            model.addAttribute("isAdd",true);
            model.addAttribute("category",category);
            return "ModifyCategory";
        }else {
            categoryService.add(category);
            page = categoryService.allPage()/5 + (categoryService.allPage()%5==0?0:1);
            return "redirect:/category";
        }
    }

    @GetMapping("/updatecategory/{id}")
    public String updateView(@PathVariable("id") int id,Model model){
        model.addAttribute("isAdd",false);
        System.out.println(categoryService.findById(id));
        model.addAttribute("category",categoryService.findById(id));
        return "ModifyCategory";
    }

    @PostMapping("/updatecategory")
    public String updateSubmit(@Valid @ModelAttribute("category") Category category, BindingResult bindingResult1, Model model){
        if(bindingResult1.hasErrors()){
            model.addAttribute("isAdd",false);
            model.addAttribute("category",category);
            return "ModifyCategory";
        }else {
            categoryService.update(category);
            return "redirect:/category";
        }
    }

    @GetMapping("/deletecategory/{id}")
    public String deleteCategory(@PathVariable("id") int id){
        System.out.println("ID " + id);
        categoryService.deleteById(id);
        return "redirect:/category";
    }

    @GetMapping("/catpage/{page}")
    public String page(@PathVariable("page") Integer page){
        CategoryController.page = page;
        return "redirect:/category";
    }
}
