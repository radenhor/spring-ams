package com.example.ams.controller;

import com.example.ams.model.Article;
import com.example.ams.model.Category;
import com.example.ams.service.ArticleService;
import com.example.ams.service.CategoryService;
import com.example.ams.service.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Controller
public class HomeController {

    private CategoryService categoryService;
    private ArticleService articleService;
    private boolean filterPage = false, filterSearch=false, isAdd = true, filterAll = true;
    public static Integer page = 0,cat_id = 0, startPage = 1, curPage = 1;
    private String title = "";
    private MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping("/")
    public String index(Model model){
        filterPage = true;
        Pagination.inc = 10;
        filterSearch = true;
        filterAll = true;
        cat_id = 0;
//        if(!isAdd) {
//            page = 1;
//            page = 0;
//        }
        page = 0;

        Integer allPage = articleService.allPage();
        List<Article> articles = articleService.findByPage(page);
        model.addAttribute("currentPage",curPage);
        model.addAttribute("searchParameter","");
        model.addAttribute("articles",articles);
        model.addAttribute("catName",true);
        model.addAttribute("pageSelect",page);
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("date",new Date());
        model.addAttribute("page",Pagination.preparePage(allPage,10));
//        model.addAttribute("listPage",Pagination.convertPage1(Pagination.preparePage(allPage,10),allPage));
        model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allPage));
        return "index";
    }

    @GetMapping("/add")
    public String addView(Model model){
       Article article = new Article();
       article.setCreatedDate(new Date().toString());
       article.setId(1);
       article.setCategory(new Category(1,""));
        model.addAttribute("isAdd",true);
        model.addAttribute("article", article);
        model.addAttribute("category",categoryService.findAll());
       return "modify";
    }

    @PostMapping("/add")
    public String addData(@Valid @ModelAttribute("article") Article article, BindingResult bindingResult, Model model, @RequestParam("file")MultipartFile file,@RequestParam("catId") int catId){
        article.setCategory(new Category(catId,""));
        System.out.println("ARTICLE : "+article.toString());
       if(bindingResult.hasErrors()){
           System.out.println("ERROR" +messageSource.getMessage(bindingResult.getFieldError(),null));
           model.addAttribute("article",article);
           model.addAttribute("category",categoryService.findAll());
           model.addAttribute("isAdd",true);
           return "modify";
       }else {
           try{
               if(!file.isEmpty()){
                   String fileName = UUID.randomUUID().toString()+file.getOriginalFilename();
                   Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir")+"/src/main/resources/image/",fileName));
//                   Files.copy(file.getInputStream(), Paths.get("http://34.73.36.238/image/",fileName));

                   article.setImage(fileName);
               }else{
                   article.setImage("cncc.png");
               }
               article.setCategory(new Category(catId,""));
               articleService.add(article);
                isAdd = true;
           }catch (IOException  e){
               e.printStackTrace();
           }
           return "redirect:add";
       }
    }

    @GetMapping("/update/{id}")
    public String updateView(Model model,@PathVariable("id") Integer id){
        Article article = articleService.findById(id);
        model.addAttribute("isAdd",false);
        model.addAttribute("article", article);
        model.addAttribute("category",categoryService.findAll());
        return "modify";
    }

    @PostMapping("/update")
    public String updateSubmit(@Valid @ModelAttribute("article") Article article,BindingResult bindingResult,Model model,@RequestParam("file")MultipartFile file,@RequestParam("catId") int catId){
       if(bindingResult.hasErrors()){
           model.addAttribute("article",article);
           return "modify";
       }else {
           try{
               if(!file.isEmpty()){
                   String fileName = UUID.randomUUID().toString()+file.getOriginalFilename();
                   Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir")+"/src/main/resources/image/",fileName));
                   article.setImage(fileName);
               }
               article.setCategory(new Category(catId,""));
               System.out.println(article.toString());
               article.setCreatedDate(new Date().toString());
               articleService.update(article);
               isAdd = true;
               if(cat_id!=0){
                    afterModify(model);
               }else {
                   if(page==0)page=1;
                   Integer allPage = articleService.allPage();
                   List<Article> articles = articleService.findByPage(page);
                   model.addAttribute("currentPage",page);
                   model.addAttribute("articles",articles);
                   model.addAttribute("pageSelect",page-1);
                   model.addAttribute("category",categoryService.findAll());
                   model.addAttribute("catName",true);
                   model.addAttribute("page",Pagination.preparePage(allPage,10));
                   model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allPage));
               }

           }catch (IOException e){
               e.printStackTrace();
           }
           return "index";
       }
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id,Model model){
       articleService.deleteById(id);
       isAdd = true;
        if(cat_id!=0){
            afterModify(model);
        }else {
            System.out.println("PAGE_SELECT : "+page);
            Integer allPage = articleService.allPage();
            List<Article> articles = articleService.findByPage(page);
            model.addAttribute("currentPage",page);
            model.addAttribute("articles",articles);
            model.addAttribute("pageSelect",Pagination.pageToIndex(""+page,Pagination.preparePage(allPage,10),allPage));
            model.addAttribute("category",categoryService.findAll());
            model.addAttribute("catName",true);
            model.addAttribute("page",Pagination.preparePage(allPage,10));
            model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allPage));
        }

        return "index";
    }

    @GetMapping("/article/{id}")
    public String article(@PathVariable Integer id,Model model){
       Article article = articleService.findById(id);
       model.addAttribute("category",categoryService.findAll());
       model.addAttribute("article",article);
        isAdd = true;
       return "article";
    }

    @GetMapping("/page/{page}")
    public String page(@PathVariable Integer page,Model model){
       isAdd = false;
       HomeController.page = page;
       if(filterPage){
           if(filterSearch){
               if(filterAll){
                   Integer allArticle = articleService.filterAllArticle(title);
                   List<Article> articles = articleService.filterAllArticleByPage(title,page);
                   model.addAttribute("category",categoryService.findAll());
                   model.addAttribute("currentPage",page);
                   model.addAttribute("searchParameter",title);
                   model.addAttribute("articles",articles);
                   model.addAttribute("pageSelect",Pagination.pageToIndex(""+page,Pagination.preparePage(allArticle,10),allArticle));
                   model.addAttribute("catId",cat_id);
                   model.addAttribute("catName",true);
                   model.addAttribute("page", Pagination.inc>(allArticle / 10 + (allArticle % 10 == 0 ? 0 : 1))?(allArticle / 10 + (allArticle % 10 == 0 ? 0 : 1)):Pagination.inc);
                   model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allArticle));
//                   model.addAttribute("listPage",Pagination.convertPage1(Pagination.inc,allArticle));
               }else {
                   System.out.println("BOOK_PAGE : "+page);
                   Integer allArticle = articleService.filterByTitle(title,cat_id);
                   List<Article> articles = articleService.filterByTitlePage(cat_id,title,page);
                   model.addAttribute("category",categoryService.findAll());
                   model.addAttribute("currentPage",page);
                   model.addAttribute("searchParameter",title);
                   model.addAttribute("articles",articles);
                   model.addAttribute("pageSelect",Pagination.pageToIndex(""+page,Pagination.preparePage(allArticle,10),allArticle));
                   //                   model.addAttribute("pageSelect",page);
                   model.addAttribute("catId",cat_id);
                   model.addAttribute("catName",false);
                   model.addAttribute("page", Pagination.preparePage(allArticle,10));
                   model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allArticle));
//                   model.addAttribute("listPage",Pagination.convertPage(Pagination.preparePage(allArticle,10)));
               }
           }else {
               Integer allArticle = articleService.filterCategory(cat_id);
               List<Article> articles = articleService.filterCategoryPage(cat_id,page);
               model.addAttribute("category",categoryService.findAll());
               model.addAttribute("currentPage",page);
               model.addAttribute("articles",articles);
               model.addAttribute("pageSelect",page);
               model.addAttribute("catName",false);
               model.addAttribute("page", Pagination.preparePage(allArticle,10));
               model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allArticle));
//               model.addAttribute("listPage",Pagination.convertPage(Pagination.preparePage(allArticle,10)));
           }
       }else {
           Integer allPage = articleService.allPage();
           List<Article> articles = articleService.findByPage(page);
           model.addAttribute("currentPage",page);
           model.addAttribute("articles",articles);
           model.addAttribute("pageSelect",page);
           model.addAttribute("category",categoryService.findAll());
           model.addAttribute("date",new Date());
           model.addAttribute("catName",false);
           model.addAttribute("page",Pagination.preparePage(allPage,10));
           model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allPage));
//           model.addAttribute("listPage",Pagination.convertPage(Pagination.preparePage(allPage,10)));
       }
       return "index";
    }

    @GetMapping("/category/{catId}")
    public String filterCategory(@PathVariable("catId") int catId,Model model){
        page = 1;
        isAdd = false;
        filterPage = true;
        filterSearch = false;
        cat_id = catId;
        Integer allArticle = articleService.filterCategory(catId);
        List<Article> articles = articleService.filterCategoryPage(cat_id,page);
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("currentPage",curPage);
        model.addAttribute("catName",false);
        model.addAttribute("articles",articles);
        model.addAttribute("pageSelect",page);
        model.addAttribute("page", Pagination.preparePage(allArticle,10));
        model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allArticle));
//        model.addAttribute("listPage",Pagination.convertPage(Pagination.preparePage(allArticle,10)));
        return "index";
    }

    @GetMapping("/article/search")
    public String filterByTitle(@RequestParam("title") String title,@RequestParam("catId") Integer catId,Model model){
        page = 1;
        filterPage = true;
        filterSearch = true;
        this.title = title;
        cat_id = catId;

        System.out.println("CAT_ID : "+catId);
        if(catId==0){
            filterAll = true;
            Pagination.inc = 10;
            Integer allArticle = articleService.filterAllArticle(title);
            List<Article> articles = articleService.filterAllArticleByPage(title,page);
            System.out.println(articles.toString());
            model.addAttribute("catName",true);
            model.addAttribute("category",categoryService.findAll());
            model.addAttribute("currentPage",1);
            model.addAttribute("searchParameter",title);
            model.addAttribute("articles",articles);
            model.addAttribute("pageSelect",0);
            model.addAttribute("catId",catId);
            model.addAttribute("page", Pagination.preparePage(allArticle,10));
            model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allArticle));
//            model.addAttribute("listPage",Pagination.convertPage(Pagination.preparePage(allArticle,10)));
        }else {
            Pagination.inc = 10;
            filterAll = false;
            model.addAttribute("catName",false);
            Integer allArticle = articleService.filterByTitle(title,catId);
            List<Article> articles = articleService.filterByTitlePage(catId,title,page);
            model.addAttribute("category",categoryService.findAll());
            model.addAttribute("currentPage",1);
            model.addAttribute("searchParameter",title);
            model.addAttribute("articles",articles);
            model.addAttribute("pageSelect",0);
            model.addAttribute("catId",catId);
            model.addAttribute("page", Pagination.preparePage(allArticle,10));
            model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allArticle));
//            model.addAttribute("listPage",Pagination.convertPage(Pagination.preparePage(allArticle,10)));
        }

       return "index";
    }

    private void afterModify(Model model){
        Integer allArticle = articleService.filterByTitle(title,cat_id);
        List<Article> articles = articleService.filterByTitlePage(cat_id,title,page);
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("currentPage",page);
        model.addAttribute("searchParameter",title);
        model.addAttribute("articles",articles);
        model.addAttribute("pageSelect",Pagination.pageToIndex(""+page,Pagination.preparePage(allArticle,10),allArticle));
        //                   model.addAttribute("pageSelect",page);
        model.addAttribute("catId",cat_id);
        model.addAttribute("catName",false);
        model.addAttribute("page", Pagination.preparePage(allArticle,10));
        model.addAttribute("enPage",Pagination.convertPage(Pagination.inc,allArticle));
    }
}
