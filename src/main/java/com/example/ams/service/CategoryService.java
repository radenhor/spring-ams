package com.example.ams.service;

import com.example.ams.model.Category;

import java.util.List;

public interface CategoryService {
    void deleteById(int id);
    List<Category> findAll();
    Category findById(int id);
    void update(Category category);
    void add(Category category);
    List<Category> findByPage(int page);
    Integer allPage();
}
