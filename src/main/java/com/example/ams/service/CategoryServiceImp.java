package com.example.ams.service;

import com.example.ams.controller.CategoryController;
import com.example.ams.controller.HomeController;
import com.example.ams.model.Article;
import com.example.ams.model.Category;
import com.example.ams.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void deleteById(int id) {

        categoryRepository.deleteById(id);
//        List<Category> categories = categoryRepository.findByPage(CategoryController.page);
        Integer allPage = categoryRepository.allPage();
        int page = (allPage / 5 + (allPage % 5 == 0 ? 0 : 1));
        System.out.println("ALLPAGE  : "+page);
//        if(allPage>=5){
//            page = allPage / 5 + (allPage%5 == 0 ? 0 : 1);
//        }
//
//        for(int i=0;i<categories.size();i++){
//            if(id==categories.get(i).getId()){
//                categories.remove(i);
//                allPage--;
//            }
//        }

        if(((allPage%5)<=0)&&(page == CategoryController.page-1)){
            CategoryController.page--;
        }
        System.out.println(CategoryController.page);

    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(int id) {
        return categoryRepository.findById(id);
    }

    @Override
    public void update(Category category) {
        categoryRepository.update(category);
    }

    @Override
    public void add(Category category) {
        categoryRepository.add(category);
    }

    @Override
    public List<Category> findByPage(int page) {
//        List<Category> categories = categoryRepository.findAll();
//        List<Category> subCategories = new ArrayList<>();
//        for(int i=(page*5)-5;i<(page*5);i++){
//            if(i==categories.size()||i<0){
//                break;
//            }
//            else {
//                subCategories.add(categories.get(i));
//            }
//
//        }
        return categoryRepository.findByPage((page-1)*5);
    }

    @Override
    public Integer allPage() {
        return categoryRepository.allPage();
    }
}
