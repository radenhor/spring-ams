create table tb_category(
  id int primary key auto_increment,
  name varchar(200),
);

create table tb_article(
  id int primary key auto_increment,
  title varchar(200),
  description varchar(200),
  author varchar(200),
  thumbnail varchar(200),
  created_date varchar(200),
  cat_id int not null references tb_category(id) on delete cascade
);

